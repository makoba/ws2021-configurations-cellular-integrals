# ws2021-configurations-cellular-integrals

These are notes for a talk I gave in the research seminar of the ESAGA in the winter term 2021 (organized by Johannes Sprang and Anneloes Viergever).
A compiled version can be found [here](https://makoba.gitlab.io/ws2021-configurations-cellular-integrals/configurations-cellular-integrals.pdf).