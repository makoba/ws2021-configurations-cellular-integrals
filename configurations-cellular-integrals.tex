\documentclass[10pt, a4paper]{scrartcl}

\usepackage{packages}
\usepackage{commands}

\title{Configurations and Cellular Integrals}
\author{Manuel Hoff}
\date{}

\begin{document}

    \maketitle

    These are notes for a talk I gave in the Research Seminar on \emph{irrationality proofs of zeta values} taking place in the winter term 2021/2022 in Essen.
    The goal of the talk was to introduce certain differential forms $\omega_{\delta}$ and functions $f_{\delta/\delta'}$ on $M_{0, S}$ to then define the so called \emph{basic cellular integrals} $I_{\delta/\delta'}(N)$.
    This is done via a descent procedure along the $\PGL_2$-torsor $(\projline)^S_* \to M_{0, S}$.

    I thank the organizers of the seminar, Johannes Sprang and Anneloes Viergever, for helping me with the preparation.

    \begin{notation}
        Let $k$ be a field.
        \begin{itemize}
            \item
            An \emph{algebraic $k$-scheme} is a $k$-scheme of finite type.

            \item
            A \emph{$k$-variety} is a geometrically integral algebraic $k$-scheme.
        \end{itemize}
        Given a $k$-variety $X$ we denote by $K(X)$ its associated field of rational functions.
        If $X$ is affine then we also write $\calO(X) \coloneqq \Gamma(X, \calO_X) \subseteq K(X)$ and $\Omega^i(X) \coloneqq \Gamma(X, \Omega^i_{X/k})$ as well as $\Omega^i(X)_{\eta} \coloneqq K(X) \otimes_{\calO(X)} \Omega^i(X)$.
        Likewise, if $X \to Y$ is a map of affine $k$-varieties, we write $\Omega^i(X/Y) \coloneqq \Gamma(X, \Omega^i_{X/Y})$.
    \end{notation}

    \section{A bit of Descent Theory} \label{sec:descent}

    In this section we fix a field $k$ and a smooth affine connected algebraic $k$-group $G$ of dimension $m$.
    Moreover $\pi \colon X \to Y$ always denotes a (left-)$G$-torsor where $X, Y$ are smooth affine $k$-varieties of dimensions $n$ and $\ell$ respectively (so that $m = n - \ell$).

    \subsection{Descent for Vector Bundles and their Sections}

    \begin{definition}
        Let $\calE \in \catvects(X)$ be a vector bundle on $X$.
        A \emph{$G$-equivariant structure for $\calE$} is a collection $(\rho_{R, g})_{R, g}$ of isomorphisms
        \[
            \rho_{R, g} \colon g^* \calE_R \cong \calE_R
        \]
        of vector bundles on $X_R$, indexed over all $k$-algebras $R$ and elements $g \in G(R)$, such that the following two conditions are satisfied:
        \begin{itemize}
            \item
            Given a map of $k$-algebras $R \to R'$ and an element $g \in G(R)$ we have $(\rho_{R, g})_{R'} = \rho_{R', g}$.

            \item
            Given a $k$-algebra $R$ and two elements $g, g' \in G(R)$ we have $\rho_{R, g g'} = \rho_{R, g'} \circ (g')^* \rho_{R, g}$.
        \end{itemize}
        We denote by $\catvects^G(X)$ the category of $G$-equivariant vector bundles on $X$ (i.e.\ vector bundles equipped with a $G$-equivariant structure).

        Now let $\calE \in \catvects^G(X)$.
        Then a section $s \in \calE(X)$ is called \emph{$G$-invariant} if we have
        \[
            g^* s = s \in g^* \calE(X_R) \cong \calE(X_R)
        \]
        for all $k$-algebras $R$ and $g \in G(R)$.
        We denote by $\calE(X)^G \subseteq \calE(X)$ the subset of all $G$-invariant sections.
        Note that this is in fact an $\calO(Y)$-submodule.
    \end{definition}

    \begin{theorem}
        We have an equivalence of categories
        \[
            \catvects(Y) \to \catvects^G(X), \qquad \calF \mapsto \pi^* \calF.
        \]
        Here $\pi^* \calF$ is implicitly equipped with a certain canonical $G$-equivariant structure.

        Moreover, given $\calF \in \catvects(Y)$, the map
        \[
            \calF(Y) \to \pi^* \calF(X), \qquad s \mapsto \pi^* s
        \]
        is an isomorphism (of $\calO(Y)$-modules) onto $\pi^* \calF(X)^G$.
    \end{theorem}

    \begin{example} \label{ex:differentials}
        The vector bundles $\Omega^i_{X/k}$ and $\Omega^i_{X/Y}$ on $X$ carry canonical $G$-equivariant structures, given by pulling back differential forms.
        We will later use the fact that the classical short exact sequence
        \[
            0 \to \pi^* \Omega^1_{Y/k} \to \Omega^1_{X/k} \to \Omega^1_{X/Y} \to 0
        \]
        is $G$-equivariant and thus gives rise to a $G$-equivariant isomorphism $\Omega^n_{X/k} \cong \pi^* \Omega^{\ell}_{Y/k} \otimes_{\calO_X} \Omega^m_{X/Y}$.
    \end{example}

    \subsection{Volume Forms}

    \begin{definition}
        A \emph{left-invariant volume form for $G$} is a nonzero element $\nu \in \Omega^m(G)^G$ (where the invariants are taken with respect to the left multiplication action of $G$ on itself).
    \end{definition}

    \begin{remark}
        As $\Omega^m_{G/k}$ is a $G$-equivariant line bundle, $\Omega^m(G)^G$ is a $1$-dimensional $k$-vector space.
        Consequently a left-invariant volume form for $G$ always exists, is unique up to a scalar in $k^{\times}$ and freely generates $\Omega^1(G)$ as an $\calO(G)$-module.
    \end{remark}

    One might wonder if there is in fact a left-invariant volume form for $G$ that is also right-invariant (such $G$ are called \emph{unimodular}).

    To analyze this question it is useful to define the \emph{modular character of $G$} (thanks to everyone who participated in making this remark during the talk).
    This is the character $\chi \colon G \to \Gm$ that is uniquely characterized by the formula $r_g^* \nu = \chi(g) \cdot \nu$ for all $k$-algebras $R$, $g \in G(R)$ and a left-invariant volume form $\nu$ for $G$.
    Here $r_g \colon G_R \to G_R$ denotes the right multiplication by $g$.
    It is now immediate that $G$ has bi-invariant volume forms if and only if $\chi$ is trivial.
    
    \begin{lemma} \label{lem:red-unimod}
        If $G$ is reductive then it is unimodular.
    \end{lemma}

    \begin{proof}
        We need to show that $\chi$ is trivial.
        For this it suffices to show that it is trivial when restricted to $Z(G)$ and $G^{\der}$ (because $Z(G) \times G^{\der} \to G$ is surjective).
        Now we make the following observations:
        \begin{itemize}
            \item
            Given $g \in Z(G)(R)$ for some $k$-algebra $R$ we have $r_g = l_g \colon G_R \to G_R$ ($l_g$ denotes left multiplication by $g$).
            Thus $r_g^* \nu = l_g^* \nu = \nu$ for a left-invariant volume form $\nu$ for $G$ and it follows that $\chi(g) = 1$.

            \item
            The group $G^{\der}$ is semisimple and hence every character $G^{\der} \to \Gm$ is trivial. \qedhere
        \end{itemize}
    \end{proof}

    \subsection{Descent for Differential Forms}

    Now assume that $G$ is unimodular and let $\nu \in \Omega^m(G)$ be a bi-invariant volume form for $G$.

    \begin{lemma}
        There exists a unique $\widetilde{\nu} \in \Omega^m(X/Y)$ that satisfies $\widetilde{\nu} = \pr_1^* \nu$ for every local trivialisation $X \cong G \times Y$.
        Moreover $\widetilde{\nu}$ is $G$-invariant and freely generates $\Omega^m(X/Y)$ as a $\calO(X)$-module.
    \end{lemma}

    \begin{proof}
        We only need to show that $\widetilde{\nu}$ exists and to do so we may even assume that the torsor $\pi \colon X \to Y$ is globally trivial.
        In this situation two global trivialisations always differ by an isomorphism $r_g \colon G \times Y \to G \times Y$ for some $g \in G(Y)$.
        Now $\pr_1^* \nu$ pulls back to itself under this map because $\nu$ is right-invariant.
    \end{proof}

    Using \Cref{ex:differentials} we now obtain a $G$-equivariant isomorphism
    \[
        \pi^* \Omega^{\ell}_{Y/k} \cong \Omega^n_{X/k}, \qquad \alpha \mapsto \alpha \wedge \nu'
    \]
    where $\nu' \in \Omega^m(X)$ is a preimage of $\widetilde{\nu}$ under $\Omega^m(X) \to \Omega^m(X/Y)$ (and this isomorphism is independent of the choice of $\nu'$).
    Thus, by taking $G$-invariant sections, we obtain a $\calO(Y)$-linear isomorphism $\Omega^n(X)^G \cong \Omega^{\ell}(Y)$ and thus a way to descend differential forms from $X$ to $Y$.

    We might ask how we can effectively compute the image of some $\widetilde{\omega} \in \Omega^n(X)^G$ in $\Omega^{\ell}(Y)$.
    For this the following lemma will come in handy.

    \begin{lemma} \label{lem:explicit}
        Suppose that $\pi \colon X \to Y$ is in fact a trivial torsor and pick a section $s \colon Y \to X$ inducing an isomorphism $X \cong G \times Y$.
        Let $\widetilde{\omega} \in \Omega^n(X)^G$ and let $\omega \in \Omega^{\ell}(Y)$ be its image.
        Also set $\nu' \coloneqq \pr_1^* \nu \in \Omega^m(X)$ (note that this maps to $\widetilde{\nu}$ under $\Omega^m(X) \to \Omega^m(X/Y)$ by definition) and suppose we are given $\omega' \in \Omega^{\ell}(X)$ such that $\omega' \wedge \nu' = \widetilde{\omega}$.

        Then we have $\omega = s^* \omega' \in \Omega^{\ell}(Y)$.
    \end{lemma}

    \begin{proof}
        The product decomposition $X \cong G \times Y$ induces a direct sum decomposition $\Omega^{\ell}(X) = \pi^* \Omega^{\ell}(Y) \oplus N$ with
        \[
            N \coloneqq \bigoplus_{i = 1}^m \pi^* \Omega^{\ell - i}(Y) \otimes_{\calO(X)} \pr_1^* \Omega^i(G)
        \]
        (where we formally set $\Omega^j(Y) = 0$ for $j < 0$).
        Now we make the following observations:
        \begin{itemize}
            \item
            $N$ is precisely the kernel of the map $\Omega^{\ell}(X) \to \Omega^n(X)$ given by $\alpha \mapsto \alpha \wedge \nu'$.

            \item
            We have $s^* \alpha = 0$ for all $\alpha \in N$ because the composition $\pr_1 \circ s$ is constant.
        \end{itemize}
        As $\omega' \wedge \nu' = \pi^* \omega \wedge \nu' = \widetilde{\omega}$, the first point implies that $\omega' - \pi^* \omega \in N$.
        The second point then implies that
        \[
            0 = s^*(\omega' - \pi^* \omega) = s^* \omega' - (\pi \circ s)^* \omega = s^* \omega' - \omega
        \]
        as desired.
    \end{proof}

    \section{Recollections}

    In the following sections all algebro-geometric objects will live over the base field $\QQ$.
    Moreover $S$ will always denote a finite set with $n \coloneqq \abs{S} \geq 3$ and we also set $\ell \coloneqq n - 3$.

    \begin{itemize}
        \item
        We defined the variety $M_{0, S}$ by setting $M_{0, S}(R)$ (for some $\QQ$-algebra $R$) to be the set of isomorphism classes of tuples $(C, (z_s)_{s \in S})$ consisting of a smooth projective curve $C/R$ of genus $0$ and points $z_s \in C(R)$ indexed by the set $S$ (note that we necessarily have $C \cong \projline_R$).
        $M_{0, S}$ is a smooth affine variety of dimension $\ell$.

        \item
        We also defined a compactification $M_{0, S} \subseteq \overline{M}_{0, S}$ that is a smooth projective variety of dimension $\ell$ such that $\overline{M}_{0, S} \setminus M_{0, S}$ is a simple normal crossing divisor.
        We call the irreducible components of $\overline{M}_{0, S} \setminus M_{0, S}$ \emph{boundary divisors}.
        $\overline{M}_{0, S}$ is a moduli space for $S$-pointed stable curves of genus $0$ (we made precise what this means in a Ludvig's talk).
        An $S$-pointed stable curve of genus $0$ over some field $k/\QQ$ has an associated tree whose inner vertices all have valency at least $3$ and whose leaves are labeled bijectively by the elements of the set $S$ (this was discussed in Andrés' talk).

        \item
        A \emph{stable partition of $S$} is an (unordered) decomposition $S = T \sqcup U$ such that $\abs{T}, \abs{U} \geq 2$.
        We have a bijection
        \[
            \curlybr[\big]{\text{stable partitions of $S$}} \cong \curlybr[\big]{\text{boundary divisors}}
        \]
        that is given by sending a stable partition to the locus of all those curves whose associated tree contains an edge that induces the specified partition.

        \item
        A \emph{dihedral structure on $S$} is a left coset $\delta \in \Bij(\anglebr{n}, S)/\Dih_{2n}$.
        Here $\anglebr{n} \coloneqq \curlybr{1, \dotsc, n}$ and $\Dih_{2n} \subseteq \Sym_n$ denotes the standard dihedral subgroup of the symmetric group on the set $\anglebr{n}$ (it is generated by the elements $(1, \dotsc, n)$ and $(1, n) (2, n-1) \dotsm$).
        More intuitively a dihedral structure on $S$ is a labeling of the edges of an unoriented $n$-gon by the elements of $S$.
        The set $\anglebr{n}$ carries a distinguished dihedral structure (given by the identity) that we denote by $\delta_0$.

        We have a bijection
        \[
            \curlybr[\big]{\text{dihedral structures on $S$}} \cong \pi_0(M_{0, S}(\RR))
        \]
        that is given by sending a dihedral structure $\delta$ to the set of points $(C, (z_s)_{s \in S}) \in M_{0, S}(\RR)$ such that the elements $z_s \in C(\RR)$ are in dihedral order with respect to $\delta$ (note that $C(\RR)$ is non-canonically homeomorphic to the circle).
        We write $X_{\delta}$ for the connected component associated to $\delta$ and we write $\overline{X}_{\delta}$ for its closure inside $\overline{M}_{0, S}(\RR)$.

        \item
        Given a dihedral structure $\delta$ on $S$ we write $\delta_f$ for the set of boundary divisors $D$ such that $\overline{X}_{\delta} \cap D(\RR) \neq \emptyset$ and call those boundary divisors \emph{of finite distance} with respect to $\delta$.
        We also write $\delta_{\infty}$ for the set of all other boundary divisors that we call \emph{of infinite distance}.
        
        If $D$ is a boundary divisor corresponding to a stable partition $S = T \sqcup U$ then $D \in \delta_f$ if and only if the sets $T$ and $U$ are consecutive with respect to $\delta$ (we call such a partition \emph{compatible} with $\delta$).

        \item
        Given $\delta$ the open $M_{0, S}^{\delta} \coloneqq \overline{M}_{0, S} \setminus \bigcup_{D \in \delta_{\infty}} D \subseteq \overline{M}_{0, S}$ is affine.
        For varying $\delta$ this then yields an affine open covering of $\overline{M}_{0, S}$.
        The $M_{0, S}^{\delta}$ have explicit affine embeddings (called dihedral coordinates) that were discussed in Chirantan's talk.

        \item
        We write $\projline$ for the projective line.
        This is the smooth projective curve with function field $K(\projline) = \QQ(z)$.
        The algebraic group $\GL_2$ acts (from the left) on $\projline$ by linear fractional transformations
        $\begin{psmallmatrix} a & b \\ c & d \end{psmallmatrix}.z \coloneqq \frac{az + b}{cz + d}$ and this action factors over $\PGL_2 = \GL_2/\Gm$.

        An important fact is that the action of $\PGL_2$ on $\projline$ is simply transitive on distinct triples of points (this might be called triply transitive).
        
        \item
        We write $(\projline)^S_* \subseteq (\projline)^S$ for the complement of the fat diagonal (of course this still carries a $\PGL_2$-action).
        This is a smooth affine variety of dimension $n$.
        We also define the map
        \[
            \pi \colon (\projline)^S_* \to M_{0, S}, \qquad (z_s)_{s \in S} \mapsto (\projline, (z_s)_{s \in S}).
        \]
        It is a (left-)torsor under $\PGL_2$.

        \item
        Define the \emph{standard simplicial chart} to be
        \[
            \simpchart \coloneqq \set[\big]{(t_1, \dotsc, t_{\ell})}{\text{$t_i \neq 0, 1$ and $t_i \neq t_j$ for $i \neq j$}} \subseteq \affspace^{\ell}.
        \]
        We have the natural map
        \begin{equation} \label{eq:simpchart}
            \simpchart \to (\projline)^n_*, \qquad (t_1, \dotsc, t_{\ell}) \mapsto (0, t_1, \dotsc, t_{\ell}, 1, \infty)
        \end{equation}
        that induces an isomorphism $\simpchart \cong M_{0, n}$.
        Consequently this gives a section of the torsor $\pi \colon (\projline)^n_* \to M_{0, n}$ and thus a product decomposition $(\projline)^n_* \cong \PGL_2 \times M_{0, n}$.
    \end{itemize}

    \section{Cellular forms}

    Our aim is now to define certain forms $\omega_{\delta} \in \Omega^{\ell}(M_{0, S})$ and regular functions $f_{\delta/\delta'} \in \calO(M_{0, S})$ associated to dihedral structures on the set $S$.
    To do this we will first define analogous objects on $(\projline)^S_*$ and then descend these along $\pi$ using the material from \Cref{sec:descent}.

    \begin{remark}
        All the forms and functions we will define in this section will only be well defined up to a sign!
    \end{remark}

    \begin{definition}
        Let $\delta, \delta'$ be dihedral structures on $S$ and pick enumerations $\beta \in \delta$ and $\beta' \in \delta'$.
        Then we set
        \[
            \widetilde{\omega}_{\delta} \coloneqq \pm \prod_{i \in \ZZ/n\ZZ} \frac{dz_{\beta(i)}}{z_{\beta(i)} - z_{\beta(i + 1)}} \in \Omega^n((\projline)^S_*)
        \]
        and
        \[
            \widetilde{f}_{\delta/\delta'} \coloneqq \pm \prod_{i \in \ZZ/n\ZZ} \frac{z_{\beta(i)} - z_{\beta(i + 1)}}{z_{\beta'(i)} - z_{\beta'(i + 1)}} \in \calO((\projline)^S_*).
        \]
    \end{definition}

    \begin{remark}
        To see that the above definition really defines regular sections we make the following observations:
        \begin{itemize}
            \item
            For $s, t \in  S$ distinct the function $\frac{1}{z_s - z_t} \in \calO((\projline)^S_*)$ has simple zeros along $z_s = \infty$ and $z_t = \infty$ and no other zeros.

            \item
            For $s \in S$ the differential $dz_s \in \Omega^1((\projline)^S_*)_{\eta}$ has no zeros, a double pole along $z_s = \infty$ and no other poles.
        \end{itemize}
        Consequently we see that the $\widetilde{\omega}_{\delta}$ and $\widetilde{f}_{\delta/\delta'}$ are well defined and in fact nowhere vanishing.
        Moreover it is clear from the formulae that they are also independent (up to the sign) of the choice of $\beta$ and $\beta'$.
    \end{remark}

    \begin{lemma} \label{lem:props}
        We have the following properties:
        \begin{itemize}
            \item
            The $\widetilde{\omega}_{\delta}$ and $\widetilde{f}_{\delta/\delta'}$ are $\PGL_2$-invariant.

            \item
            $\widetilde{f}_{\delta/\delta'} \cdot \widetilde{f}_{\delta'/\delta''} = \pm \widetilde{f}_{\delta/\delta''}$ and $\widetilde{f}_{\delta/\delta'} \cdot \widetilde{\omega}_{\delta} = \pm \widetilde{\omega}_{\delta'}$.

            \item
            Given a bijection of finite sets $\varphi \colon S_1 \cong S_2$ with $\abs{S_1} = \abs{S_2} = n$ and dihedral structures $\delta, \delta'$ on $S_1$ we have $(\varphi^*)^* \widetilde{\omega}_{\delta} = \pm \widetilde{\omega}_{\varphi \delta}$ and $(\varphi^*)^* \widetilde{f}_{\delta/\delta'} = \pm \widetilde{f}_{\varphi \delta/\varphi \delta'}$ (here $\varphi^* \colon (\projline)^{S_2}_* \cong (\projline)^{S_1}_*$ denotes the isomorphism induced by $\varphi$ going in the reverse direction).
        \end{itemize}
    \end{lemma}

    \begin{proof}
        The second and third properties are immediately checked.
        For the first property it suffices to show that $g^* \widetilde{\omega}_{\delta} = \widetilde{\omega}_{\delta}$ and $g^* \widetilde{f}_{\delta/\delta'} = \widetilde{f}_{\delta/\delta'}$ for the matrices $g$ given by
        \[
            \begin{pmatrix} 0 & 1 \\ 1 & 0 \end{pmatrix} \in \GL_2(\QQ), \quad
            \begin{pmatrix} 1 & a \\ 0 & 1 \end{pmatrix} \in \GL_2(\QQ[a]), \quad
            \begin{pmatrix} a & 0 \\ 0 & 1 \end{pmatrix} \in \GL_2(\QQ[a^{\pm 1}]),
        \]
        because these already generate $\GL_2$ (thanks to Vytautas Paškūnas for pointing this out).
        The corresponding linear fractional transformations are
        \[
            z \mapsto z^{-1}, \hspace{6em} z \mapsto z + a, \hspace{6em} z \mapsto a z.
        \]
        For the second and third case, it is almost immediate to check the invariance for both $\widetilde{\omega}_{\delta}$ and $\widetilde{f}_{\delta/\delta'}$ thus we only consider the first one.
        But calculating $d(z^{-1}) = - z^{-2} \cdot dz$ and $z_s^{-1} - z_t^{-1} = (z_s z_t)^{-1} \cdot (z_t - z_s)$ also gives the invariance in that case.
    \end{proof}

    Now pick a bi-invariant volume form $\nu$ on $\PGL_2$ (this is possible because $\PGL_2$ is reductive, even adjoint, and hence unimodular by \Cref{lem:red-unimod}) and normalize it so that $\widetilde{\nu} = \pm \widetilde{\omega}_{\delta}$ for $\abs{S} = 3$.
    Using the material from \Cref{sec:descent} we obtain isomorphisms $\Omega^n((\projline)^S_*)^{\PGL_2} \cong \Omega^{\ell}(M_{0, S})$ and $\calO((\projline)^S_*)^{\PGL_2} \cong \calO(M_{0, S})$ and can define the $\ell$-forms $\omega_{\delta}$ and the regular functions $f_{\delta/\delta'}$ on $M_{0, S}$ as the images of their corresponding tilde-versions under these isomorphisms.
    The chosen normalization then tautologically gives $\omega_{\delta} = \pm 1$ for $\abs{S} = 3$ and the relations from the second and third point of \Cref{lem:props} also hold for the non-tilde-versions.

    \begin{remark}
        Suppose we are given two dihedral structures $\delta, \delta'$ on $\anglebr{n}$ and pick enumerations $\beta \in \delta$ and $\beta' \in \delta'$ such that $\beta(n) = \beta'(n) = n$.
        One can ask for an explicit description of $\omega_{\delta}$ and $f_{\delta/\delta'}$ in terms of the isomorphism $\simpchart \cong M_{0, n}$.
        \begin{itemize}
            \item
            For $f_{\delta/\delta'}$ this is easy.
            We just have to compute the pullback of $\widetilde{f}_{\delta/\delta'}$ along the map $\simpchart \to (\projline)^n_*$ from \Cref{eq:simpchart}.
            This gives
            \[
                f_{\delta/\delta'} = \pm \prod_{i = 1}^{n - 2} \frac{z_{\beta(i)} - z_{\beta(i + 1)}}{z_{\beta'(i)} - z_{\beta'(i + 1)}}
            \]
            where we formally set
            \[
                z_i \coloneqq
                \begin{cases}
                    0 & i = 1, \\
                    1 & i = n - 1, \\
                    t_{i - 1} & i = 2, \dotsc, n - 2.
                \end{cases}
            \]

            \item
            For $\omega_{\delta}$ we have to work a little more but we can follow the recipe of \Cref{lem:explicit}.
            We use the section $s \colon \simpchart \to (\projline)^n_*$ and obtain a decomposition $(\projline)^n_* \cong \PGL_2 \times \simpchart$.
            The projection $\pr_1 \colon (\projline)^n_* \to \PGL_2$ then identifies with the projection
            \[
                (\projline)^n_* \to (\projline)^3_*, \qquad (z_1, \dotsc, z_n) \mapsto (z_1, z_{n-1}, z_n)
            \]
            under the isomorphism
            \[
                \PGL_2 \cong (\projline)^3_*, \qquad g \mapsto g.(0, 1, \infty).
            \]
            Thus $\nu'$ is given by
            \[
                \nu' = \pm \frac{dz_1 \wedge dz_{n-1} \wedge dz_n}{(z_1 - z_{n-1}) \cdot (z_{n-1} - z_n) \cdot (z_n - z_1)}.
            \]
            We guess
            \[
                \omega'_{\delta} \coloneqq \pm \frac{(z_{n - 1} - z_n) \cdot (z_n - z_1)}{(z_{\beta(n-1)} - z_n) \cdot (z_n - z_{\beta(1)})} \cdot \frac{(z_1 - z_{n-1}) \cdot dz_2 \wedge \dotsm \wedge dz_{n - 2}}{\prod_{i = 1}^{n - 2} (z_{\beta(i)} - z_{\beta(i + 1)})}
            \]
            and check that $\omega'_{\delta} \wedge \nu' = \widetilde{\omega}_{\delta}$ as desired.

            This then finally gives that $\omega_{\delta} = s^* \omega'_{\delta}$ which we calculate to be
            \[
                \omega_{\delta} = \pm \frac{dt_1 \wedge \dotsm \wedge dt_l}{\prod_{i = 1}^{n - 2} (z_{\beta(i)} - z_{\beta(i + 1)})}
            \]
            where we use the same convention as in the calculation of $f_{\delta/\delta'}$.
            In the last step of the calculation we have used that
            \[
                s^* \frac{(z_{n - 1} - z_n) \cdot (z_n - z_1)}{(z_{\beta(n-1)} - z_n) \cdot (z_n - z_{\beta(1)})} = 1
            \]
            and that $s^* (z_1 - z_{n - 1}) = -1$ so that these terms disappear.
            Thanks to Anneloes Viergever for figuring out this computation with me!
        \end{itemize}
    \end{remark}

    \section{Cellular Integrals and Configurations}

    \begin{definition}
        We make the following definitions:

        \begin{itemize}
            \item
            A \emph{configuration} is a triple $(S, \delta, \delta')$ with $\abs{S} = n \geq 3$ and $\delta, \delta'$ two dihedral structures on $S$.

            \item
            We say that two configurations $(S_1, \delta_1, \delta'_1)$ and $(S_2, \delta_2, \delta'_2)$ are \emph{equivalent} if there exists a bijection $\varphi \colon S_1 \cong S_2$ such that $\varphi \delta_1 = \delta_2$ and $\varphi \delta'_1 = \delta'_2$.
            The set of all equivalence classes of configurations (of a given cardinality $n$) is denoted by $\Conf_n$.
            In the following we will be sloppy about distinguishing between configurations and their equivalence classes.

            \item
            A configuration $(S, \delta, \delta') \in \Conf_n$ is called \emph{convergent} if we have $\delta_f \cap \delta'_f = \emptyset$.
            This means that there is no stable partition of $S$ that is compatible both with $\delta$ and $\delta'$.

            \item
            Given a configuration $(S, \delta, \delta') \in \Conf_n$ and an integer $N \in \ZZ_{\geq 0}$ we define the associated \emph{basic cellular integral}
            \[
                I_{\delta/\delta'}(N) \coloneqq \abs[\Big]{\int_{X_{\delta}} (f_{\delta/\delta'})^N \omega_{\delta'}} \in \RR_{> 0} \cup \curlybr{\infty}.
            \]
            As we are taking the absolute value anyway this is independent of any chosen sign and independent of the chosen orientation on $X_{\delta}$ used for carrying out the integration.
            Moreover note that $(f_{\delta/\delta'})^N \omega_{\delta'}$ is nowhere vanishing and thus has constant sign on $X_{\delta}$ (again with respect to some orientation) so that the expression is well defined (although we don't make any claim about convergence).

            This gives a map $\Conf_n \times \ZZ_{\geq 0} \to \RR_{> 0} \cup \curlybr{\infty}$.
        \end{itemize}
    \end{definition}

    These basic cellular integrals are our main object of interest and the whole reason why we talked about the moduli spaces $M_{0, S}$ and dihedral structures.
    In the next talk we will first be concerned with characterizing those configurations whose associated cellular integrals converge (and these will turn out to be precisely the convergent ones, thus explaining the terminology).
    It should also be noted that this combinatorial property of being a convergent configuration is precisely the generalized dinner table problem: Given $n$ guests sitting at a round table, what are the possibilities of reseating them in such a way that no set of $k$ guests (for $2 \leq k \leq n - 2$) that were sitting consecutively before reseating also sit consecutively after reseating, all up to any sorts of symmetries of the table and the guests?

    We finish by giving a concrete description of the set $\Conf_n$.

    \begin{lemma}
        The map
        \[
            \Dih_{2n} \backslash \Sym_n / \Dih_{2n} \to \Conf_n, \qquad \Dih_{2n} \sigma \Dih_{2n} \mapsto (\anglebr{n}, \delta_0, \sigma \delta_0)
        \]
        is a well defined bijection.
    \end{lemma}
    
    \begin{proof}
        To see that the map is well defined, suppose we are given $\sigma \in \Sym_n$ and $\tau, \tau' \in \Dih_{2n}$.
        Then we have
        \[
            (\anglebr{n}, \delta_0, \tau \sigma \tau' \delta_0) = (\anglebr{n}, \delta_0, \tau \sigma \delta_0) = (\anglebr{n}, \tau^{-1} \delta_0, \sigma \delta_0) = (\anglebr{n}, \delta_0, \sigma \delta_0)
        \]
        where we use that $\tau' \delta_0 = \tau^{-1} \delta_0 = \delta_0$ in the first and last step and translate by $\tau^{-1}$ in the second step.
        The map is clearly surjective as for any $(S, \delta, \delta') \in \Conf_n$ we can choose an enumeration $\beta \in \delta$ that we can use to obtain
        \[
            (S, \delta, \delta') = (\anglebr{n}, \delta_0, \beta^{-1} \delta').
        \]
        To see that the map is injective, suppose that we have $\sigma, \sigma' \in \Sym_n$ that map to the same element in $\Conf_n$.
        This means that there exists yet another permutation $\tau \in \Sym_n$ such that $\tau \delta_0 = \delta_0$ and $\tau \sigma \delta_0 = \sigma' \delta_0$.
        The first condition gives that $\tau \in \Dih_{2n}$ and the second condition implies that there exists $\tau' \in \Dih_{2n}$ such that $\tau \sigma \tau' = \sigma'$ as desired.
    \end{proof}

\end{document}
